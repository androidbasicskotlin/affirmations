package com.franck.affirmations

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import com.franck.affirmations.adapter.ItemAdapter
import com.franck.affirmations.data.Datasource

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /*val textView: TextView = findViewById(R.id.textview)
        val numberOfAffirmation = Datasource().loadAffirmations().size.toString()
        textView.text = "Number of affirmation: $numberOfAffirmation"*/

        // We create an instance of Datasource
        val myDataset = Datasource().loadAffirmations()

        //create a variable for recyclerView
        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)

        // Initialize the adapter property of the recyclerView
        recyclerView.adapter = ItemAdapter(this,myDataset)

        // As we know in advance that the layout size of the RecyclerView
        // is fixed in the activity layout and that changes in content do not
        //change the layout size of the RecyclerView

        recyclerView.setHasFixedSize(true)
    }
}