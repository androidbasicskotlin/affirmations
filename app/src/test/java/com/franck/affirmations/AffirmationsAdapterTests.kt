package com.franck.affirmations

import android.content.Context
import com.franck.affirmations.adapter.ItemAdapter
import com.franck.affirmations.model.Affirmation
import org.junit.Assert.assertEquals
import org.junit.Test

import org.mockito.Mockito.mock

class AffirmationsAdapterTests {

    // We create a mocked instance of a context since unit tests don't run a device
    // but run on the Java Virtual Machine (JVM)
    private val context = mock(Context::class.java)

    /**
     * We want to make sure that the size of the adapter
     * is the size of the list that passed to the adapter.
     */
    @Test
    fun adapter_size(): Unit {
        // our list of affirmations for test
        val data = listOf(
            Affirmation(R.string.affirmation1, R.drawable.image1),
            Affirmation(R.string.affirmation2, R.drawable.image2)
        )

        // We create an instance of ItemAdapter
        val adapter = ItemAdapter(context, data)

        // finally the test
        assertEquals("ItemAdapter is not the correct size", data.size, adapter.itemCount)
    }
}