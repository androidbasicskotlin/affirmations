package com.franck.affirmations

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

// Add a test runner to the newly created class. @RunWith(AndroidJUnit4::class)

@RunWith(AndroidJUnit4::class)
class AffirmationsListTests {

    // Make an activity scenario rule for the main activity.
    // We launch the activity before doing any test.
    @get:Rule
    val activity = ActivityScenarioRule(MainActivity::class.java)

    /**
     * This test should scroll to a specific item contained in the list.
     *
     * We annotate with @Test to tell that it's a test
     */

    @Test
    fun scroll_to_item(){
        onView(withId(R.id.recycler_view)).perform(
            RecyclerViewActions
                .scrollToPosition<RecyclerView.ViewHolder>(9))

        onView(withText(R.string.affirmation10))
            .check(matches(isDisplayed()))
    }




}